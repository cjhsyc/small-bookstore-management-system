package dao;

import entity.OutOfStock;
import util.JdbcUtil;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OutOfStockDao {
    public static List<OutOfStock> selectOutOfStock() throws SQLException, ClassNotFoundException {
        List<OutOfStock> outOfStockList=new ArrayList<>();
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st=connection.prepareCall("{call selectOutOfStock()}");
        ResultSet resultSet= st.executeQuery();
        while (resultSet.next()){
            OutOfStock outOfStock=new OutOfStock();
            outOfStock.setOutOfStockID(resultSet.getInt("outOfStockID"));
            outOfStock.setBookID(resultSet.getInt("bookID"));
            outOfStock.setNumber(resultSet.getInt("number"));
            outOfStockList.add(outOfStock);
        }
        st.close();
        connection.close();
        return outOfStockList;
    }
}
