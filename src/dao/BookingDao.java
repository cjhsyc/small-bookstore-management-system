package dao;

import entity.Booking;
import entity.Client_Booking;
import util.JdbcUtil;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookingDao {
    public static void insertBooking(Integer clientID,Integer bookID,Integer number) throws SQLException, ClassNotFoundException {
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call insertBooking(?,?,?)}");
        st.setInt("clientID",clientID);
        st.setInt("bookID",bookID);
        st.setInt("number",number);
        st.executeUpdate();
        st.close();
        connection.close();
    }

    public static List<Client_Booking> selectClient_Booking() throws SQLException, ClassNotFoundException {
        List<Client_Booking> client_bookingList=new ArrayList<>();
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call selectClient_Booking()}");
        ResultSet resultSet=st.executeQuery();
        while (resultSet.next()){
            Client_Booking client_booking=new Client_Booking();
            client_booking.setBookingID(resultSet.getInt("bookingID"));
            client_booking.setClientName(resultSet.getString("clientName"));
            client_booking.setBookName(resultSet.getString("bookName"));
            client_booking.setNumber(resultSet.getInt("number"));
            client_bookingList.add(client_booking);
        }
        st.close();
        connection.close();
        return client_bookingList;
    }

    public static Booking selectBookingByID(Integer bookingID) throws SQLException, ClassNotFoundException {
        Booking booking=new Booking();
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call selectBookingByID(?)}");
        st.setInt("bookingID",bookingID);
        ResultSet resultSet= st.executeQuery();
        while (resultSet.next()){
            booking.setBookingID(resultSet.getInt("bookingID"));
            booking.setBookID(resultSet.getInt("bookID"));
            booking.setClientID(resultSet.getInt("clientID"));
            booking.setNumber(resultSet.getInt("number"));
        }
        st.close();
        connection.close();
        return booking;
    }

    public static void deleteBooking(Integer bookingID) throws SQLException, ClassNotFoundException {
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call deleteBooking(?)}");
        st.setInt("bookingID",bookingID);
        st.executeUpdate();
        st.close();
        connection.close();
    }
}
