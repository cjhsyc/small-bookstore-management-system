package dao;

import entity.Order;
import entity.Supplier;
import util.JdbcUtil;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SupplierDao {
    public static Supplier selectSupplierByID(Integer supplierID) throws SQLException, ClassNotFoundException {
        Supplier supplier=new Supplier();
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call selectSupplierByID(?)}");
        st.setInt("supplierID",supplierID);
        ResultSet resultSet= st.executeQuery();
        while (resultSet.next()){
            supplier.setSupplierID(resultSet.getInt("supplierID"));
            supplier.setName(resultSet.getString("name"));
        }
        st.close();
        connection.close();
        return supplier;
    }

    public static List<Supplier> selectSupplier() throws SQLException, ClassNotFoundException {
        List<Supplier> supplierList=new ArrayList<>();
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call selectSupplier()}");
        ResultSet resultSet=st.executeQuery();
        while (resultSet.next()){
            Supplier supplier=new Supplier();
            supplier.setSupplierID(resultSet.getInt("supplierID"));
            supplier.setName(resultSet.getString("name"));
            supplier.setPhoneNumber(resultSet.getString("phoneNumber"));
            supplierList.add(supplier);
        }
        st.close();
        connection.close();
        return supplierList;
    }

    public static void insertSupplier(String name,String phoneNumber) throws SQLException, ClassNotFoundException {
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call insertSupplier(?,?)}");
        st.setString("name",name);
        st.setString("phoneNumber",phoneNumber);
        st.executeUpdate();
        st.close();
        connection.close();
    }

    public static Supplier selectSupplierByName(String name) throws SQLException, ClassNotFoundException {
        Supplier supplier=new Supplier();
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call selectSupplierByName(?)}");
        st.setString("name",name);
        ResultSet resultSet=st.executeQuery();
        while (resultSet.next()){
            supplier.setSupplierID(resultSet.getInt("supplierID"));
            supplier.setName(resultSet.getString("name"));
        }
        st.close();
        connection.close();
        return supplier;
    }

    public static void deleteSupplier(Integer supplierID) throws SQLException, ClassNotFoundException {
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call deleteSupplier(?)}");
        st.setInt("supplierID",supplierID);
        st.executeUpdate();
        st.close();
        connection.close();
    }
}
