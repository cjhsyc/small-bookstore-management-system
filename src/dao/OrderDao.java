package dao;

import com.sun.org.apache.xpath.internal.operations.Or;
import entity.Order;
import util.JdbcUtil;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class OrderDao {
    public static void insertOrder(Integer bookID,Integer number,String time,Double cost) throws SQLException, ClassNotFoundException {
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call insertOrder(?,?,?,?)}");
        st.setInt("bookID",bookID);
        st.setInt("number",number);
        st.setString("time",time);
        st.setDouble("cost",cost);
        st.executeUpdate();
        st.close();
        connection.close();
    }

    public static void insertOrderByBooking(Integer bookID,Integer number,Integer clientID,String time,Double cost) throws SQLException, ClassNotFoundException {
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call insertOrderByBooking(?,?,?,?,?)}");
        st.setInt("bookID",bookID);
        st.setInt("number",number);
        st.setInt("clientID",clientID);
        st.setString("time",time);
        st.setDouble("cost",cost);
        st.executeUpdate();
        st.close();
        connection.close();
    }

    public static List<Order> selectOrder() throws SQLException, ClassNotFoundException {
        List<Order> orderList=new ArrayList<>();
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call selectOrder()}");
        ResultSet resultSet=st.executeQuery();
        while (resultSet.next()){
            Order order=new Order();
            order.setOrderID(resultSet.getInt("orderID"));
            order.setBookID(resultSet.getInt("bookID"));
            order.setClientID(resultSet.getInt("clientID"));
            order.setNumber(resultSet.getInt("number"));
            order.setTime(resultSet.getString("time"));
            order.setCost(resultSet.getDouble("cost"));
            orderList.add(order);
        }
        st.close();
        connection.close();
        return orderList;
    }
}
