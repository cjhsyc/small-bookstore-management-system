package dao;

import entity.Client;
import util.JdbcUtil;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientDao {
    public static Client selectClientByPhoneNumber(String phoneNumber) throws SQLException, ClassNotFoundException {
        Client client=new Client();
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call selectClientByPhoneNumber(?)}");
        st.setString("phoneNumber",phoneNumber);
        ResultSet resultSet=st.executeQuery();
        while (resultSet.next()){
            client.setClientID(resultSet.getInt("clientID"));
            client.setName(resultSet.getString("name"));
            client.setPhoneNumber(resultSet.getString("phoneNumber"));
        }
        st.close();
        connection.close();
        return client;
    }

    public static List<Client> selectClient() throws SQLException, ClassNotFoundException {
        List<Client> clientList=new ArrayList<>();
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call selectClient()}");
        ResultSet resultSet=st.executeQuery();
        while (resultSet.next()){
            Client client=new Client();
            client.setClientID(resultSet.getInt("clientID"));
            client.setPhoneNumber(resultSet.getString("phoneNumber"));
            client.setName(resultSet.getString("name"));
            clientList.add(client);
        }
        st.close();
        connection.close();
        return clientList;
    }

    public static void insertClient(String name,String phoneNumber) throws SQLException, ClassNotFoundException {
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call insertClient(?,?)}");
        st.setString("name",name);
        st.setString("phoneNumber",phoneNumber);
        st.executeUpdate();
        st.close();
        connection.close();
    }

    public static Client selectClientByID(Integer clientID) throws SQLException, ClassNotFoundException {
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call selectClientByID(?)}");
        st.setInt("clientID",clientID);
        ResultSet resultSet= st.executeQuery();
        Client client=new Client();
        while (resultSet.next()){
            client.setClientID(resultSet.getInt("clientID"));
            client.setName(resultSet.getString("name"));
            client.setPhoneNumber(resultSet.getString("phoneNumber"));
        }
        st.close();
        connection.close();
        return client;
    }

    public static void deleteClient(Integer clientID) throws SQLException, ClassNotFoundException {
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call deleteClient(?)}");
        st.setInt("clientID",clientID);
        st.executeUpdate();
        st.close();
        connection.close();
    }
}
