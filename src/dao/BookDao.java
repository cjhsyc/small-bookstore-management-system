package dao;

import entity.Book;
import util.JdbcUtil;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookDao {
    public static List<Book> selectBook() throws SQLException, ClassNotFoundException {
        List<Book> bookList=new ArrayList<>();
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call selectBook()}");
        ResultSet resultSet=st.executeQuery();
        while(resultSet.next()){
            Book book=new Book();
            book.setBookID(resultSet.getInt("bookID"));
            book.setName(resultSet.getString("name"));
            book.setPrice(resultSet.getDouble("price"));
            book.setSupplierID(resultSet.getInt("supplierID"));
            book.setDiscount(resultSet.getDouble("discount"));
            book.setNumber(resultSet.getInt("number"));
            bookList.add(book);
        }
        st.close();
        connection.close();
        return bookList;
    }

    public static Book selectBookByID(Integer bookID) throws SQLException, ClassNotFoundException {
        Book book=new Book();
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call selectBookByID(?)}");
        st.setInt("bookID",bookID);
        ResultSet resultSet=st.executeQuery();
        while(resultSet.next()){
            book.setBookID(resultSet.getInt("bookID"));
            book.setName(resultSet.getString("name"));
            book.setPrice(resultSet.getDouble("price"));
            book.setSupplierID(resultSet.getInt("supplierID"));
            book.setDiscount(resultSet.getDouble("discount"));
            book.setNumber(resultSet.getInt("number"));
        }
        return book;
    }

    public static void updateBook(Integer bookID,String name,Double price,Double discount,Integer number) throws SQLException, ClassNotFoundException {
        Connection connection= JdbcUtil.getConnection();
        CallableStatement st = connection.prepareCall("{call updateBook(?,?,?,?,?)}");
        st.setInt("bookID",bookID);
        st.setString("name",name);
        st.setDouble("discount",discount);
        st.setDouble("price",price);
        st.setInt("number",number);
        st.executeUpdate();
        st.close();
        connection.close();
    }

    public static void insertBook(Integer bookID,String name,Double price,Integer supplierID) throws SQLException, ClassNotFoundException {
        Connection connection=JdbcUtil.getConnection();
        CallableStatement st=connection.prepareCall("{call insertBook(?,?,?,?)}");
        st.setInt("bookID",bookID);
        st.setString("name",name);
        st.setDouble("price",price);
        st.setInt("supplierID",supplierID);
        st.executeUpdate();
        st.close();
        connection.close();
    }

    public static void deleteBook(Integer bookID) throws SQLException, ClassNotFoundException {
        Connection connection=JdbcUtil.getConnection();
        CallableStatement st=connection.prepareCall("{call deleteBook(?)}");
        st.setInt("bookID",bookID);
        st.executeUpdate();
        st.close();
        connection.close();
    }
}
