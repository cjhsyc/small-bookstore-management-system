/*
 * Created by JFormDesigner on Tue Dec 21 16:10:01 CST 2021
 */

package win;

import entity.MessageModel;
import service.BackendService;
import service.ClientService;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import javax.swing.*;
import javax.swing.table.*;

/**
 * @author unknown
 */
public class ClientInformationWin extends JFrame {
    public ClientInformationWin(Object[][] objects) {
        this.objects=objects;
        initComponents();
    }

    private void button2ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        String name=JOptionPane.showInputDialog(this, "请输入姓名", "提示", JOptionPane.PLAIN_MESSAGE);
        String phoneNumber=JOptionPane.showInputDialog(this, "请输入电话号码", "提示", JOptionPane.PLAIN_MESSAGE);
        MessageModel messageModel= ClientService.clientRegister(name,phoneNumber);
        JOptionPane.showMessageDialog(null,messageModel.getMessage(),"提示",JOptionPane.PLAIN_MESSAGE);
        Object[][] objects= BackendService.getClientList();
        ClientInformationWin win=new ClientInformationWin(objects);
        win.setVisible(true);
        dispose();
    }

    private void button1ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        String str=JOptionPane.showInputDialog(this, "请输入客户编号", "提示", JOptionPane.PLAIN_MESSAGE);
        Integer clientID=Integer.parseInt(str);
        MessageModel messageModel=ClientService.deleteClient(clientID);
        JOptionPane.showMessageDialog(null,messageModel.getMessage(),"提示",JOptionPane.PLAIN_MESSAGE);
        Object[][] objects= BackendService.getClientList();
        ClientInformationWin win=new ClientInformationWin(objects);
        win.setVisible(true);
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        scrollPane1 = new JScrollPane();

        //======== this ========
        setTitle("\u5ba2\u6237\u4fe1\u606f");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(25, 20, 420, 255);

        contentPane.setPreferredSize(new Dimension(480, 395));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
        table1 = new JTable();
        button1 = new JButton();
        button2 = new JButton();
        //---- table1 ----
        table1.setModel(new DefaultTableModel(
                objects,
                new String[] {
                        "\u5ba2\u6237\u7f16\u53f7", "\u7535\u8bdd\u53f7\u7801", "\u59d3\u540d"
                }
        ));
        scrollPane1.setViewportView(table1);
        //---- button1 ----
        button1.setText("\u5220\u9664");
        button1.addActionListener(e -> {
            try {
                button1ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        contentPane.add(button1);
        button1.setBounds(95, 300, 78, 30);
        //---- button2 ----
        button2.setText("\u6ce8\u518c");
        button2.addActionListener(e -> {
            try {
                button2ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        contentPane.add(button2);
        button2.setBounds(285, 300, 78, 30);
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JScrollPane scrollPane1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    private JTable table1;
    private JButton button1;
    private JButton button2;
    private Object[][] objects;
}
