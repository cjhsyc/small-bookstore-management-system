/*
 * Created by JFormDesigner on Tue Dec 21 14:37:25 CST 2021
 */

package win;

import entity.MessageModel;
import service.BackendService;
import service.BookService;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import javax.swing.*;
import javax.swing.table.*;

/**
 * @author unknown
 */
public class BookingWin extends JFrame {
    public BookingWin(Object[][] objects) {
        this.objects = objects;
        initComponents();
    }

    private void button1ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        Integer bookingID=Integer.parseInt(textField1.getText());
        MessageModel messageModel=BookService.bookSale2(bookingID);
        if (messageModel.getCode()==1){
            JOptionPane.showMessageDialog(null, messageModel.getMessage(), "提示", JOptionPane.PLAIN_MESSAGE);
            Object[][] objects= BackendService.getClient_BookingList();
            BookingWin win=new BookingWin(objects);
            win.setVisible(true);
            dispose();
        }else {
            JOptionPane.showMessageDialog(null, messageModel.getMessage(), "提示", JOptionPane.PLAIN_MESSAGE);
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        label1 = new JLabel();
        textField1 = new JTextField();

        //======== this ========
        setTitle("\u9884\u5b9a\u4fe1\u606f");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- label1 ----
        label1.setText("\u9884\u8ba2\u7f16\u53f7\uff1a");
        contentPane.add(label1);
        label1.setBounds(new Rectangle(new Point(70, 280), label1.getPreferredSize()));
        contentPane.add(textField1);
        textField1.setBounds(145, 275, 110, 30);

        contentPane.setPreferredSize(new Dimension(525, 380));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
        button1 = new JButton();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();
        //======== scrollPane1 ========
        {

            //---- table1 ----
            table1.setModel(new DefaultTableModel(
                    objects,
                    new String[]{
                            "\u9884\u8ba2\u7f16\u53f7", "\u5ba2\u6237\u540d", "\u4e66\u540d", "\u6570\u91cf"
                    }
            ));
            scrollPane1.setViewportView(table1);
        }
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(35, 15, scrollPane1.getPreferredSize().width, 235);
        //---- button1 ----
        button1.setText("\u552e\u51fa");
        button1.addActionListener(e -> {
            try {
                button1ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        contentPane.add(button1);
        button1.setBounds(305, 275, 78, 30);
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JLabel label1;
    private JTextField textField1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    private JButton button1;
    private JScrollPane scrollPane1;
    private JTable table1;
    private Object[][] objects;
}
