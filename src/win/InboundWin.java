/*
 * Created by JFormDesigner on Mon Dec 20 23:00:45 CST 2021
 */

package win;

import service.BookService;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import javax.swing.*;

/**
 * @author unknown
 */
public class InboundWin extends JFrame {
    public InboundWin() {
        initComponents();
    }

    private void button1ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        Integer bookID=Integer.parseInt(textField1.getText());
        String name=textField2.getText();
        Double price=Double.parseDouble(textField3.getText());
        Integer supplierID=Integer.parseInt(textField4.getText());

        String str= BookService.addBook(bookID,name,price,supplierID);
        JOptionPane.showMessageDialog(null, str, "提示", JOptionPane.PLAIN_MESSAGE);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        label1 = new JLabel();
        label2 = new JLabel();
        label3 = new JLabel();
        label4 = new JLabel();
        textField1 = new JTextField();
        textField2 = new JTextField();
        textField3 = new JTextField();
        textField4 = new JTextField();

        //======== this ========
        setTitle("\u56fe\u4e66\u5165\u5e93");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- label1 ----
        label1.setText("\u56fe\u4e66\u7f16\u53f7\uff1a");
        contentPane.add(label1);
        label1.setBounds(new Rectangle(new Point(65, 40), label1.getPreferredSize()));

        //---- label2 ----
        label2.setText("\u4e66\u540d\uff1a");
        contentPane.add(label2);
        label2.setBounds(new Rectangle(new Point(70, 75), label2.getPreferredSize()));

        //---- label3 ----
        label3.setText("\u5355\u4ef7\uff1a");
        contentPane.add(label3);
        label3.setBounds(new Rectangle(new Point(70, 110), label3.getPreferredSize()));

        //---- label4 ----
        label4.setText("\u4f9b\u5e94\u5546\u7f16\u53f7\uff1a");
        contentPane.add(label4);
        label4.setBounds(new Rectangle(new Point(55, 145), label4.getPreferredSize()));
        contentPane.add(textField1);
        textField1.setBounds(145, 35, 160, 30);
        contentPane.add(textField2);
        textField2.setBounds(145, 70, 160, 30);
        contentPane.add(textField3);
        textField3.setBounds(145, 105, 160, 30);
        contentPane.add(textField4);
        textField4.setBounds(145, 140, 160, 30);

        contentPane.setPreferredSize(new Dimension(400, 300));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
        button1 = new JButton();
        //---- button1 ----
        button1.setText("\u5165\u5e93");
        button1.addActionListener(e -> {
            try {
                button1ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        contentPane.add(button1);
        button1.setBounds(150, 200, 78, 30);
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private JLabel label4;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    private JButton button1;
}
