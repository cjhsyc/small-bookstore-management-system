/*
 * Created by JFormDesigner on Mon Dec 20 14:29:16 CST 2021
 */

package win;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * @author unknown
 */
public class HomeWin extends JFrame {
    public HomeWin() {
        initComponents();
    }

    private void button1ActionPerformed(ActionEvent e) {
        // TODO add your code here
        ClientLoginWin win=new ClientLoginWin();
        win.setVisible(true);
        dispose();
    }

    private void button2ActionPerformed(ActionEvent e) {
        // TODO add your code here
        AdminLoginWin win=new AdminLoginWin();
        win.setVisible(true);
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        label1 = new JLabel();
        button1 = new JButton();
        button2 = new JButton();

        //======== this ========
        setFont(new Font(Font.DIALOG, Font.PLAIN, 12));
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- label1 ----
        label1.setText("\u5c0f\u578b\u4e66\u5e97\u7ba1\u7406\u7cfb\u7edf");
        label1.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 28));
        contentPane.add(label1);
        label1.setBounds(70, 45, 260, 55);

        //---- button1 ----
        button1.setText("\u5ba2\u6237\u5165\u53e3");
        button1.addActionListener(e -> button1ActionPerformed(e));
        contentPane.add(button1);
        button1.setBounds(45, 135, 120, 30);

        //---- button2 ----
        button2.setText("\u7ba1\u7406\u5458\u5165\u53e3");
        button2.addActionListener(e -> button2ActionPerformed(e));
        contentPane.add(button2);
        button2.setBounds(210, 135, 120, 30);

        contentPane.setPreferredSize(new Dimension(385, 255));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JLabel label1;
    private JButton button1;
    private JButton button2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
