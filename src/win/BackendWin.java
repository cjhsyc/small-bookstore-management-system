/*
 * Created by JFormDesigner on Mon Dec 20 21:14:20 CST 2021
 */

package win;

import entity.Booking;
import entity.MessageModel;
import service.BackendService;
import service.BookService;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import javax.swing.*;

/**
 * @author unknown
 */
public class BackendWin extends JFrame {
    public BackendWin() {
        initComponents();
    }

    private void button2ActionPerformed(ActionEvent e) {
        // TODO add your code here
        AdminLoginWin win = new AdminLoginWin();
        win.setVisible(true);
        dispose();
    }

    private void button1ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        Integer bookID = Integer.parseInt(textField1.getText());
        Integer number = Integer.parseInt(textField2.getText());
        MessageModel messageModel = BookService.bookSale1(bookID, number);
        JOptionPane.showMessageDialog(null, messageModel.getMessage(), "提示", JOptionPane.PLAIN_MESSAGE);
    }

    private void menuItem1ActionPerformed(ActionEvent e) {
        // TODO add your code here
        InboundWin win=new InboundWin();
        win.setVisible(true);
    }

    private void menuItem2ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        Object[][] objects= BackendService.getBookList();
        BookWin win=new BookWin(objects);
        win.setVisible(true);
    }

    private void menuItem4ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        Object[][] objects=BackendService.getClient_BookingList();
        BookingWin win=new BookingWin(objects);
        win.setVisible(true);
    }

    private void menuItem7ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        Object[][] objects=BackendService.getClientList();
        ClientInformationWin win=new ClientInformationWin(objects);
        win.setVisible(true);
    }

    private void menuItem6ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        Object[][] objects=BackendService.getOrderList();
        OrderWin win=new OrderWin(objects);
        win.setVisible(true);
    }

    private void menuItem3ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        Object[][] objects=BackendService.getSupplierList();
        SupplierWin win=new SupplierWin(objects);
        win.setVisible(true);
    }

    private void menuItem5ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        Object[][] objects=BackendService.getOutOfStockList();
        OutOfStockWin win=new OutOfStockWin(objects);
        win.setVisible(true);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        menuBar1 = new JMenuBar();
        menu1 = new JMenu();
        menuItem1 = new JMenuItem();
        label1 = new JLabel();
        textField1 = new JTextField();
        label2 = new JLabel();
        textField2 = new JTextField();
        button2 = new JButton();

        //======== this ========
        setTitle("\u540e\u53f0");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //======== menuBar1 ========
        {

            //======== menu1 ========
            {
                menu1.setText("\u83dc\u5355");

                //---- menuItem1 ----
                menuItem1.setText("\u56fe\u4e66\u5165\u5e93");
                menuItem1.addActionListener(e -> menuItem1ActionPerformed(e));
                menu1.add(menuItem1);
            }
            menuBar1.add(menu1);
        }
        setJMenuBar(menuBar1);

        //---- label1 ----
        label1.setText("\u56fe\u4e66\u7f16\u53f7\uff1a");
        contentPane.add(label1);
        label1.setBounds(new Rectangle(new Point(55, 50), label1.getPreferredSize()));
        contentPane.add(textField1);
        textField1.setBounds(135, 45, 140, 30);

        //---- label2 ----
        label2.setText("\u6570\u91cf\uff1a");
        contentPane.add(label2);
        label2.setBounds(new Rectangle(new Point(60, 105), label2.getPreferredSize()));
        contentPane.add(textField2);
        textField2.setBounds(135, 95, 140, 30);

        //---- button2 ----
        button2.setText("\u8fd4\u56de");
        button2.addActionListener(e -> button2ActionPerformed(e));
        contentPane.add(button2);
        button2.setBounds(185, 165, 78, 30);

        contentPane.setPreferredSize(new Dimension(365, 280));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
        menuItem2 = new JMenuItem();
        menuItem3 = new JMenuItem();
        menuItem4 = new JMenuItem();
        menuItem5 = new JMenuItem();
        menuItem6 = new JMenuItem();
        menuItem7 = new JMenuItem();
        button1 = new JButton();
        //---- button1 ----
        button1.setText("\u552e\u51fa");
        button1.addActionListener(e -> {
            try {
                button1ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        contentPane.add(button1);
        button1.setBounds(90, 165, 78, 30);
        //---- menuItem2 ----
        menuItem2.setText("\u56fe\u4e66\u7ba1\u7406");
        menuItem2.addActionListener(e -> {
            try {
                menuItem2ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        menu1.add(menuItem2);
        //---- menuItem3 ----
        menuItem3.setText("\u4f9b\u5e94\u5546\u4fe1\u606f");
        menuItem3.addActionListener(e -> {
            try {
                menuItem3ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        menu1.add(menuItem3);
        //---- menuItem4 ----
        menuItem4.setText("\u9884\u5b9a\u4fe1\u606f");
        menuItem4.addActionListener(e -> {
            try {
                menuItem4ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        menu1.add(menuItem4);
        //---- menuItem5 ----
        menuItem5.setText("\u7f3a\u8d27\u4fe1\u606f");
        menuItem5.addActionListener(e -> {
            try {
                menuItem5ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        menu1.add(menuItem5);
        //---- menuItem6 ----
        menuItem6.setText("\u9500\u552e\u8bb0\u5f55");
        menuItem6.addActionListener(e -> {
            try {
                menuItem6ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        menu1.add(menuItem6);
        //---- menuItem7 ----
        menuItem7.setText("客户信息");
        menuItem7.addActionListener(e -> {
            try {
                menuItem7ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        menu1.add(menuItem7);
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JMenuBar menuBar1;
    private JMenu menu1;
    private JMenuItem menuItem1;
    private JLabel label1;
    private JTextField textField1;
    private JLabel label2;
    private JTextField textField2;
    private JButton button2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    private JMenuItem menuItem2;
    private JMenuItem menuItem3;
    private JMenuItem menuItem4;
    private JMenuItem menuItem5;
    private JMenuItem menuItem6;
    private JMenuItem menuItem7;
    private JButton button1;
}
