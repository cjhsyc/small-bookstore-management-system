/*
 * Created by JFormDesigner on Tue Dec 21 16:40:44 CST 2021
 */

package win;

import entity.MessageModel;
import service.BackendService;
import util.StringUtil;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import javax.swing.*;
import javax.swing.table.*;

/**
 * @author unknown
 */
public class SupplierWin extends JFrame {
    public SupplierWin(Object[][] objects) {
        this.objects=objects;
        initComponents();
    }

    private void button1ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        MessageModel messageModel=new MessageModel();
        String name=JOptionPane.showInputDialog(this, "请输入供应商名称", "提示", JOptionPane.PLAIN_MESSAGE);
        String phoneNumber=JOptionPane.showInputDialog(this, "请输入供应商联系电话", "提示", JOptionPane.PLAIN_MESSAGE);
        if (StringUtil.isEmpty(name) || StringUtil.isEmpty(phoneNumber)) {
            JOptionPane.showMessageDialog(null,"输入不能为空","提示",JOptionPane.PLAIN_MESSAGE);
            return;
        }
        messageModel= BackendService.addSupplier(name,phoneNumber);
        if (messageModel.getCode()==1){
            JOptionPane.showMessageDialog(null,messageModel.getMessage(),"提示",JOptionPane.PLAIN_MESSAGE);
            Object[][] objects=BackendService.getSupplierList();
            SupplierWin win=new SupplierWin(objects);
            win.setVisible(true);
            dispose();
        }else {
            JOptionPane.showMessageDialog(null,messageModel.getMessage(),"提示",JOptionPane.PLAIN_MESSAGE);
        }
    }

    private void button2ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        String str=JOptionPane.showInputDialog(this, "请输入供应商编号", "提示", JOptionPane.PLAIN_MESSAGE);
        Integer supplierID=Integer.parseInt(str);
        MessageModel messageModel=BackendService.deleteSupplier(supplierID);
        if (messageModel.getCode()==1){
            JOptionPane.showMessageDialog(null,messageModel.getMessage(),"提示",JOptionPane.PLAIN_MESSAGE);
            Object[][] objects=BackendService.getSupplierList();
            SupplierWin win=new SupplierWin(objects);
            win.setVisible(true);
            dispose();
        }else {
            JOptionPane.showMessageDialog(null,messageModel.getMessage(),"提示",JOptionPane.PLAIN_MESSAGE);
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        scrollPane1 = new JScrollPane();

        //======== this ========
        setTitle("\u4f9b\u5e94\u5546\u4fe1\u606f");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(20, 20, 355, 215);

        contentPane.setPreferredSize(new Dimension(400, 350));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
        button1 = new JButton();
        button2 = new JButton();
        table1 = new JTable();
        //---- table1 ----a
        table1.setModel(new DefaultTableModel(
                objects,
                new String[] {
                        "\u4f9b\u5e94\u5546\u7f16\u53f7", "\u540d\u79f0","联系电话"
                }
        ));
        scrollPane1.setViewportView(table1);
        //---- button1 ----
        button1.setText("\u6dfb\u52a0");
        button1.addActionListener(e -> {
            try {
                button1ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        contentPane.add(button1);
        button1.setBounds(70, 260, 78, 30);
        //---- button2 ----
        button2.setText("\u5220\u9664");
        button2.addActionListener(e -> {
            try {
                button2ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        contentPane.add(button2);
        button2.setBounds(215, 260, 78, 30);
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JScrollPane scrollPane1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    private JButton button1;
    private JButton button2;
    private JTable table1;
    private Object[][] objects;
}
