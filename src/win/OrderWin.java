/*
 * Created by JFormDesigner on Tue Dec 21 16:40:15 CST 2021
 */

package win;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

/**
 * @author unknown
 */
public class OrderWin extends JFrame {
    public OrderWin(Object[][] objects) {
        this.objects=objects;
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        scrollPane1 = new JScrollPane();

        //======== this ========
        setTitle("\u8ba2\u5355\u4fe1\u606f");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(20, 10, 750, 375);

        contentPane.setPreferredSize(new Dimension(790, 445));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
        table1 = new JTable();
        //---- table1 ----
        table1.setModel(new DefaultTableModel(
                objects,
                new String[] {
                        "\u8ba2\u5355\u7f16\u53f7", "\u56fe\u4e66\u7f16\u53f7", "\u6570\u91cf", "\u5ba2\u6237\u7f16\u53f7", "\u65f6\u95f4", "\u8d39\u7528"
                }
        ));
        scrollPane1.setViewportView(table1);
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JScrollPane scrollPane1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    private JTable table1;
    private Object[][] objects;
}
