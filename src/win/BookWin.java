/*
 * Created by JFormDesigner on Mon Dec 20 23:48:22 CST 2021
 */

package win;

import entity.MessageModel;
import service.BackendService;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.function.DoubleUnaryOperator;
import javax.swing.*;
import javax.swing.table.*;

/**
 * @author unknown
 */
public class BookWin extends JFrame {
    public BookWin(Object[][] objects) {
        this.objects = objects;
        initComponents();
    }

    private void button2ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        String str = JOptionPane.showInputDialog(this, "请输入图书编号", "提示", JOptionPane.PLAIN_MESSAGE);
        Integer bookID = Integer.parseInt(str);
        str = JOptionPane.showInputDialog(this, "请输入添加数量", "提示", JOptionPane.PLAIN_MESSAGE);
        Integer number = Integer.parseInt(str);
        MessageModel messageModel = BackendService.addBookNumber(bookID, number);
        if (messageModel.getCode() == 1) {
            JOptionPane.showMessageDialog(null, messageModel.getMessage(), "提示", JOptionPane.PLAIN_MESSAGE);
            Object[][] objects = BackendService.getBookList();
            BookWin win = new BookWin(objects);
            win.setVisible(true);
            dispose();
        } else {
            JOptionPane.showMessageDialog(null, messageModel.getMessage(), "提示", JOptionPane.PLAIN_MESSAGE);
        }
    }

    private void button1ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        String str = JOptionPane.showInputDialog(this, "请输入图书编号", "提示", JOptionPane.PLAIN_MESSAGE);
        Integer bookID = Integer.parseInt(str);
        int i = JOptionPane.showConfirmDialog(null, "确认要删除该编号图书？", "提示", JOptionPane.YES_NO_OPTION);
        if (i==JOptionPane.YES_OPTION){
            MessageModel messageModel=BackendService.deleteBook(bookID);
            if (messageModel.getCode()==1){
                JOptionPane.showMessageDialog(null, messageModel.getMessage(), "提示", JOptionPane.PLAIN_MESSAGE);
                Object[][] objects = BackendService.getBookList();
                BookWin win = new BookWin(objects);
                win.setVisible(true);
                dispose();
            }else{
                JOptionPane.showMessageDialog(null, messageModel.getMessage(), "提示", JOptionPane.PLAIN_MESSAGE);
            }
        }
    }

    private void button3ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        String str = JOptionPane.showInputDialog(this, "请输入图书编号", "提示", JOptionPane.PLAIN_MESSAGE);
        Integer bookID = Integer.parseInt(str);
        str = JOptionPane.showInputDialog(this, "请输入添加数量", "提示", JOptionPane.PLAIN_MESSAGE);
        Double discount = Double.parseDouble(str);
        MessageModel messageModel = BackendService.updateDiscount(bookID, discount);
        if (messageModel.getCode() == 1) {
            JOptionPane.showMessageDialog(null, messageModel.getMessage(), "提示", JOptionPane.PLAIN_MESSAGE);
            Object[][] objects = BackendService.getBookList();
            BookWin win = new BookWin(objects);
            win.setVisible(true);
            dispose();
        } else {
            JOptionPane.showMessageDialog(null, messageModel.getMessage(), "提示", JOptionPane.PLAIN_MESSAGE);
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown

        //======== this ========
        setTitle("\u56fe\u4e66\u7ba1\u7406");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        contentPane.setPreferredSize(new Dimension(770, 450));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
        button3 = new JButton();
        button1 = new JButton();
        button2 = new JButton();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();
        //======== scrollPane1 ========
        {

            //---- table1 ----
            table1.setModel(new DefaultTableModel(
                    objects,
                    new String[]{
                            "\u7f16\u53f7", "\u4e66\u540d", "\u5355\u4ef7", "\u4f9b\u5e94\u5546", "\u6298\u6263", "\u5e93\u5b58"
                    }
            ));
            scrollPane1.setViewportView(table1);
        }
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(50, 20, 670, 270);
        //---- button1 ----
        button1.setText("\u5220\u9664\u56fe\u4e66");
        button1.addActionListener(e -> {
            try {
                button1ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        contentPane.add(button1);
        button1.setBounds(125, 325, 120, 30);
        //---- button2 ----
        button2.setText("\u6dfb\u52a0\u5e93\u5b58");
        button2.addActionListener(e -> {
            try {
                button2ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        contentPane.add(button2);
        button2.setBounds(325, 325, 120, 30);
        //---- button3 ----
        button3.setText("\u4fee\u6539\u6298\u6263");
        button3.addActionListener(e -> {
            try {
                button3ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        contentPane.add(button3);
        button3.setBounds(525, 325, 120, 30);
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JScrollPane scrollPane1;
    private JTable table1;
    private Object[][] objects;
}
