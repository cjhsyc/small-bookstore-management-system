/*
 * Created by JFormDesigner on Mon Dec 20 15:40:34 CST 2021
 */

package win;

import entity.MessageModel;
import service.BookService;
import service.ClientService;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import javax.swing.*;
import javax.swing.table.*;

/**
 * @author unknown
 */
public class ClientWin extends JFrame {
    public ClientWin(Integer clientID,Object[][] objects) {
        this.clientID=clientID;
        this.objects=objects;
        initComponents();

    }

    private void button2ActionPerformed(ActionEvent e) {
        // TODO add your code here
        ClientLoginWin win=new ClientLoginWin();
        win.setVisible(true);
        dispose();
    }

    private void button1ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        Integer bookID=Integer.parseInt(textField1.getText());
        Integer number=Integer.parseInt(textField2.getText());
        MessageModel messageModel= ClientService.clientBooking(clientID,bookID,number);
        if(messageModel.getCode()==1){
            JOptionPane.showMessageDialog(null,messageModel.getMessage(),"提示",JOptionPane.PLAIN_MESSAGE);
        }else {
            JOptionPane.showMessageDialog(null,messageModel.getMessage(),"提示",JOptionPane.PLAIN_MESSAGE);
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        label1 = new JLabel();
        textField1 = new JTextField();
        label2 = new JLabel();
        textField2 = new JTextField();
        button2 = new JButton();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- label1 ----
        label1.setText("\u7f16\u53f7\uff1a");
        contentPane.add(label1);
        label1.setBounds(new Rectangle(new Point(75, 390), label1.getPreferredSize()));
        contentPane.add(textField1);
        textField1.setBounds(120, 385, 75, 30);

        //---- label2 ----
        label2.setText("\u6570\u91cf\uff1a");
        contentPane.add(label2);
        label2.setBounds(new Rectangle(new Point(220, 390), label2.getPreferredSize()));
        contentPane.add(textField2);
        textField2.setBounds(270, 385, 75, 30);

        //---- button2 ----
        button2.setText("\u8fd4\u56de");
        button2.addActionListener(e -> button2ActionPerformed(e));
        contentPane.add(button2);
        button2.setBounds(510, 385, 78, 30);

        contentPane.setPreferredSize(new Dimension(685, 505));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
        scrollPane1 = new JScrollPane();
        table1 = new JTable();
        button1 = new JButton();
        //---- button1 ----
        button1.setText("\u9884\u8ba2");
        button1.addActionListener(e -> {
            try {
                button1ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        contentPane.add(button1);
        button1.setBounds(390, 385, 78, 30);
        //======== scrollPane1 ========
        {
            //---- table1 ----
            table1.setModel(new DefaultTableModel(
                    objects,
                    new String[] {
                            "\u7f16\u53f7", "\u4e66\u540d", "\u539f\u4ef7", "\u73b0\u4ef7"
                    }
            ));
            scrollPane1.setViewportView(table1);
        }
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(45, 20, 585, 335);
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JLabel label1;
    private JTextField textField1;
    private JLabel label2;
    private JTextField textField2;
    private JButton button2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    private JScrollPane scrollPane1;
    private JTable table1;
    private Integer clientID;
    private Object[][] objects;
    private JButton button1;
}
