/*
 * Created by JFormDesigner on Mon Dec 20 15:30:07 CST 2021
 */

package win;

import com.sun.deploy.util.SessionState;
import entity.MessageModel;
import service.ClientService;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import javax.swing.*;

/**
 * @author unknown
 */
public class RegisterWin extends JFrame {
    public RegisterWin() {
        initComponents();
    }

    private void button2ActionPerformed(ActionEvent e) {
        // TODO add your code here
        ClientLoginWin win=new ClientLoginWin();
        win.setVisible(true);
        dispose();
    }

    private void button1ActionPerformed(ActionEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        String name=textField1.getText();
        String phoneNumber=textField2.getText();
        MessageModel messageModel= ClientService.clientRegister(name,phoneNumber);
        JOptionPane.showMessageDialog(null,messageModel.getMessage(),"提示",JOptionPane.PLAIN_MESSAGE);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        label1 = new JLabel();
        label2 = new JLabel();
        textField1 = new JTextField();
        textField2 = new JTextField();
        button2 = new JButton();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- label1 ----
        label1.setText("\u59d3\u540d\uff1a");
        contentPane.add(label1);
        label1.setBounds(new Rectangle(new Point(115, 70), label1.getPreferredSize()));

        //---- label2 ----
        label2.setText("\u7535\u8bdd\u53f7\u7801\uff1a");
        contentPane.add(label2);
        label2.setBounds(new Rectangle(new Point(100, 115), label2.getPreferredSize()));
        contentPane.add(textField1);
        textField1.setBounds(175, 65, 105, 30);
        contentPane.add(textField2);
        textField2.setBounds(175, 110, 105, 30);

        //---- button2 ----
        button2.setText("\u8fd4\u56de");
        button2.addActionListener(e -> button2ActionPerformed(e));
        contentPane.add(button2);
        button2.setBounds(210, 185, 78, 30);

        contentPane.setPreferredSize(new Dimension(400, 310));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
        button1 = new JButton();
        //---- button1 ----
        button1.setText("\u6ce8\u518c");
        button1.addActionListener(e -> {
            try {
                button1ActionPerformed(e);
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        contentPane.add(button1);
        button1.setBounds(100, 185, 78, 30);
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JLabel label1;
    private JLabel label2;
    private JTextField textField1;
    private JTextField textField2;
    private JButton button2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    private JButton button1;
}
