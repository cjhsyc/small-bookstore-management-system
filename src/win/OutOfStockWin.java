/*
 * Created by JFormDesigner on Tue Dec 21 17:48:37 CST 2021
 */

package win;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

/**
 * @author unknown
 */
public class OutOfStockWin extends JFrame {
    public OutOfStockWin(Object[][] objects) {
        this.objects=objects;
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        scrollPane1 = new JScrollPane();

        //======== this ========
        setTitle("\u7f3a\u8d27\u4fe1\u606f");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(15, 15, 435, 315);

        contentPane.setPreferredSize(new Dimension(470, 395));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
        table1 = new JTable();
        //---- table1 ----
        table1.setModel(new DefaultTableModel(
                objects,
                new String[] {
                        "\u7f3a\u8d27\u7f16\u53f7", "\u56fe\u4e66\u7f16\u53f7", "\u6570\u91cf"
                }
        ));
        scrollPane1.setViewportView(table1);
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JScrollPane scrollPane1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    private JTable table1;
    private Object[][] objects;
}
