/*
 * Created by JFormDesigner on Mon Dec 20 20:57:52 CST 2021
 */

package win;

import service.BackendService;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * @author unknown
 */
public class AdminLoginWin extends JFrame {
    public AdminLoginWin() {
        initComponents();
    }

    private void button1ActionPerformed(ActionEvent e) {
        // TODO add your code here
        String password = textField1.getText();
        if (BackendService.adminLogin(password)) {
            BackendWin win = new BackendWin();
            win.setVisible(true);
            dispose();
        } else {
            JOptionPane.showMessageDialog(null, "登录失败！密码错误！", "提示", JOptionPane.PLAIN_MESSAGE);
        }
    }

    private void button2ActionPerformed(ActionEvent e) {
        // TODO add your code here
        HomeWin win = new HomeWin();
        win.setVisible(true);
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        label1 = new JLabel();
        textField1 = new JTextField();
        button1 = new JButton();
        button2 = new JButton();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- label1 ----
        label1.setText("\u7ba1\u7406\u5458\u5bc6\u7801\uff1a");
        contentPane.add(label1);
        label1.setBounds(new Rectangle(new Point(70, 60), label1.getPreferredSize()));
        contentPane.add(textField1);
        textField1.setBounds(150, 55, 175, textField1.getPreferredSize().height);

        //---- button1 ----
        button1.setText("\u767b\u5f55");
        button1.addActionListener(e -> button1ActionPerformed(e));
        contentPane.add(button1);
        button1.setBounds(new Rectangle(new Point(110, 130), button1.getPreferredSize()));

        //---- button2 ----
        button2.setText("\u8fd4\u56de");
        button2.addActionListener(e -> {
            button2ActionPerformed(e);
        });
        contentPane.add(button2);
        button2.setBounds(225, 130, 78, 30);

        contentPane.setPreferredSize(new Dimension(425, 270));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JLabel label1;
    private JTextField textField1;
    private JButton button1;
    private JButton button2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
