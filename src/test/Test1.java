package test;

import service.BookService;
import util.JdbcUtil;
import win.ClientWin;

import java.sql.Connection;
import java.sql.SQLException;

public class Test1 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        //获取数据库连接
        /*Connection connection= JdbcUtil.getConnection();
        System.out.println(connection);*/

        //客户窗口
        /*ClientWin win=new ClientWin();
        win.setVisible(true);*/

        //图书信息二维数组
        Object[][] objects=BookService.getBookList();
        for(int i=0;i<objects.length;i++){
            for(int j=0;j<4;j++){
                System.out.println(objects[i][j]);
            }
        }
    }
}
