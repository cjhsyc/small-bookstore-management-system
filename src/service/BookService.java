package service;

import dao.BookDao;
import dao.BookingDao;
import dao.OrderDao;
import dao.SupplierDao;
import entity.Book;
import entity.Booking;
import entity.MessageModel;
import entity.Supplier;
import org.omg.CORBA.ORB;

import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class BookService {
    public static Object[][] getBookList() throws SQLException, ClassNotFoundException {
        List<Book> bookList;
        bookList= BookDao.selectBook();
        Object[] objects=bookList.toArray();
        Object[][] objects1=new Object[objects.length][4];
        for(int i=0;i< objects.length;i++){
            Book book= (Book) objects[i];
            objects1[i][0]=book.getBookID();
            objects1[i][1]=book.getName();
            objects1[i][2]=book.getPrice();
            objects1[i][3]=book.getPrice()*(1-book.getDiscount());
        }
        return objects1;
    }

    public static MessageModel bookSale1(Integer bookID,Integer number) throws SQLException, ClassNotFoundException {//无预订售卖
        MessageModel messageModel=new MessageModel();
        Book book=BookDao.selectBookByID(bookID);
        if(book.getBookID()==null){
            messageModel.setCode(0);
            messageModel.setMessage("出售失败！图书编号错误！");
            return messageModel;
        }
        if (number>book.getNumber()){
            messageModel.setCode(0);
            messageModel.setMessage("出售失败！库存不足！");
            return messageModel;
        }
        BookDao.updateBook(bookID,book.getName(),book.getPrice(),book.getDiscount(),book.getNumber()-number);
        String pattern="{0}";
        MessageFormat messageFormat=new MessageFormat(pattern);
        Locale locale=Locale.getDefault();
        messageFormat.setLocale(locale);
        Object[] msgParam={new Date()};
        OrderDao.insertOrder(bookID,number,messageFormat.format(msgParam),book.getPrice()*(1-book.getDiscount())*number);
        messageModel.setCode(1);
        messageModel.setMessage("出售成功！总计花费"+book.getPrice()*(1-book.getDiscount())*number+"元。");
        return messageModel;
    }

    public static String addBook(Integer bookID,String name,Double price,Integer supplierID) throws SQLException, ClassNotFoundException {
        Book book=BookDao.selectBookByID(bookID);
        if(book.getBookID() != null){
            return "入库失败！该编号的图书已入库！";
        }
        Supplier supplier= SupplierDao.selectSupplierByID(supplierID);
        if(supplier.getSupplierID()==null){
            return "入库失败！供应商编号错误！";
        }
        BookDao.insertBook(bookID,name,price,supplierID);
        return "入库成功！";
    }

    public static MessageModel bookSale2(Integer bookingID) throws SQLException, ClassNotFoundException {//通过预订出售
        MessageModel messageModel=new MessageModel();
        Booking booking= BookingDao.selectBookingByID(bookingID);
        if (booking.getBookID()==null){
            messageModel.setCode(0);
            messageModel.setMessage("出售失败！预订编号错误！");
            return messageModel;
        }
        Book book=BookDao.selectBookByID(booking.getBookID());
        if (book.getNumber()<booking.getNumber()){
            messageModel.setCode(0);
            messageModel.setMessage("出售失败！库存不足！");
            return messageModel;
        }
        BookDao.updateBook(book.getBookID(),book.getName(),book.getPrice(),book.getDiscount(),book.getNumber()-booking.getNumber());
        String pattern="{0}";
        MessageFormat messageFormat=new MessageFormat(pattern);
        Locale locale=Locale.getDefault();
        messageFormat.setLocale(locale);
        Object[] msgParam={new Date()};
        OrderDao.insertOrderByBooking(booking.getBookID(),booking.getNumber(),booking.getClientID(),messageFormat.format(msgParam),book.getPrice()*(1-book.getDiscount())*booking.getNumber());
        BookingDao.deleteBooking(bookingID);
        messageModel.setCode(1);
        messageModel.setMessage("出售成功！总计花费"+book.getPrice()*(1-book.getDiscount())*booking.getNumber()+"元。");
        return messageModel;
    }
}
