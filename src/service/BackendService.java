package service;

import dao.*;
import entity.*;

import java.lang.reflect.Member;
import java.sql.SQLException;
import java.util.List;
import java.util.regex.Pattern;

public class BackendService {
    public static boolean adminLogin(String password) {
        return password.equals("123123");
    }

    public static Object[][] getBookList() throws SQLException, ClassNotFoundException {
        List<Book> bookList;
        bookList = BookDao.selectBook();
        Object[] objects = bookList.toArray();
        Object[][] objects1 = new Object[objects.length][6];
        for (int i = 0; i < objects.length; i++) {
            Book book = (Book) objects[i];
            objects1[i][0] = book.getBookID();
            objects1[i][1] = book.getName();
            objects1[i][2] = book.getPrice();
            objects1[i][3] = book.getSupplierID();
            objects1[i][4] = book.getDiscount();
            objects1[i][5] = book.getNumber();
        }
        return objects1;
    }

    public static MessageModel addBookNumber(Integer bookID, Integer number) throws SQLException, ClassNotFoundException {
        MessageModel messageModel = new MessageModel();
        Book book = BookDao.selectBookByID(bookID);
        if (book.getBookID() == null) {
            messageModel.setCode(0);
            messageModel.setMessage("添加失败！图书编号错误！");
            return messageModel;
        }
        BookDao.updateBook(bookID, book.getName(), book.getPrice(), book.getDiscount(), book.getNumber() + number);
        messageModel.setCode(1);
        messageModel.setMessage("添加成功！");
        return messageModel;
    }

    public static MessageModel deleteBook(Integer bookID) throws SQLException, ClassNotFoundException {
        MessageModel messageModel = new MessageModel();
        Book book = BookDao.selectBookByID(bookID);
        if (book.getBookID() == null) {
            messageModel.setCode(0);
            messageModel.setMessage("删除失败！图书编号错误！");
            return messageModel;
        }
        BookDao.deleteBook(bookID);
        messageModel.setCode(1);
        messageModel.setMessage("删除成功！");
        return messageModel;
    }

    public static MessageModel updateDiscount(Integer bookID, Double discount) throws SQLException, ClassNotFoundException {
        MessageModel messageModel = new MessageModel();
        Book book = BookDao.selectBookByID(bookID);
        if (book.getBookID() == null) {
            messageModel.setCode(0);
            messageModel.setMessage("修改失败！图书编号错误！");
            return messageModel;
        }
        BookDao.updateBook(bookID, book.getName(), book.getPrice(), discount, book.getNumber());
        messageModel.setCode(1);
        messageModel.setMessage("修改成功！");
        return messageModel;
    }

    public static Object[][] getClient_BookingList() throws SQLException, ClassNotFoundException {
        List<Client_Booking> client_bookingList;
        client_bookingList = BookingDao.selectClient_Booking();
        Object[] objects = client_bookingList.toArray();
        Object[][] objects1 = new Object[objects.length][4];
        for (int i = 0; i < objects.length; i++) {
            Client_Booking client_booking = (Client_Booking) objects[i];
            objects1[i][0] = client_booking.getBookingID();
            objects1[i][1] = client_booking.getClientName();
            objects1[i][2] = client_booking.getBookName();
            objects1[i][3] = client_booking.getNumber();
        }
        return objects1;
    }

    public static Object[][] getClientList() throws SQLException, ClassNotFoundException {
        List<Client> clientList;
        clientList = ClientDao.selectClient();
        Object[] objects = clientList.toArray();
        Object[][] objects1 = new Object[objects.length][3];
        for (int i = 0; i < objects.length; i++) {
            Client client = (Client) objects[i];
            objects1[i][0] = client.getClientID();
            objects1[i][1] = client.getPhoneNumber();
            objects1[i][2] = client.getName();
        }
        return objects1;
    }

    public static Object[][] getOrderList() throws SQLException, ClassNotFoundException {
        List<Order> orderList;
        orderList = OrderDao.selectOrder();
        Object[] objects = orderList.toArray();
        Object[][] objects1 = new Object[objects.length][6];
        for (int i = 0; i < objects.length; i++) {
            Order order = (Order) objects[i];
            objects1[i][0] = order.getOrderID();
            objects1[i][1] = order.getBookID();
            objects1[i][2] = order.getNumber();
            objects1[i][3] = order.getClientID();
            objects1[i][4] = order.getTime();
            objects1[i][5] = order.getCost();
        }
        return objects1;
    }

    public static Object[][] getSupplierList() throws SQLException, ClassNotFoundException {
        List<Supplier> supplierList;
        supplierList = SupplierDao.selectSupplier();
        Object[] objects = supplierList.toArray();
        Object[][] objects1 = new Object[objects.length][3];
        for (int i = 0; i < objects.length; i++) {
            Supplier supplier = (Supplier) objects[i];
            objects1[i][0] = supplier.getSupplierID();
            objects1[i][1] = supplier.getName();
            objects1[i][2] = supplier.getPhoneNumber();
        }
        return objects1;
    }

    public static MessageModel addSupplier(String name,String phoneNumber) throws SQLException, ClassNotFoundException {
        MessageModel messageModel = new MessageModel();
        boolean b = Pattern.matches("^1[3-9][0-9]{9}$", phoneNumber);
        if (!b){
            messageModel.setCode(0);
            messageModel.setMessage("电话号码格式错误！");
            return messageModel;
        }

        Supplier supplier = SupplierDao.selectSupplierByName(name);
        if (supplier.getSupplierID() != null) {
            messageModel.setCode(0);
            messageModel.setMessage("添加失败！该供应商已存在！");
            return messageModel;
        }
        SupplierDao.insertSupplier(name,phoneNumber);
        messageModel.setCode(1);
        messageModel.setMessage("添加成功！");
        return messageModel;
    }

    public static MessageModel deleteSupplier(Integer supplierID) throws SQLException, ClassNotFoundException {
        MessageModel messageModel=new MessageModel();
        Supplier supplier=SupplierDao.selectSupplierByID(supplierID);
        if (supplier.getSupplierID()==null){
            messageModel.setCode(0);
            messageModel.setMessage("删除失败！供应商编号错误！");
            return messageModel;
        }
        SupplierDao.deleteSupplier(supplierID);
        messageModel.setCode(1);
        messageModel.setMessage("删除成功！");
        return messageModel;
    }

    public static Object[][] getOutOfStockList() throws SQLException, ClassNotFoundException {
        List<OutOfStock> outOfStockList;
        outOfStockList=OutOfStockDao.selectOutOfStock();
        Object[] objects=outOfStockList.toArray();
        Object[][] objects1=new Object[objects.length][3];
        for (int i = 0; i < objects.length; i++){
            OutOfStock outOfStock= (OutOfStock) objects[i];
            objects1[i][0] = outOfStock.getOutOfStockID();
            objects1[i][1] = outOfStock.getBookID();
            objects1[i][2] = outOfStock.getNumber();
        }
        return objects1;
    }
}
