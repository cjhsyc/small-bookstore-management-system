package service;

import dao.BookDao;
import dao.BookingDao;
import dao.ClientDao;
import entity.Book;
import entity.Client;
import entity.MessageModel;
import util.StringUtil;

import java.sql.SQLException;
import java.util.regex.Pattern;

public class ClientService {
    public static MessageModel clientLogin(String name, String phoneNumber) throws SQLException, ClassNotFoundException {
        MessageModel messageModel = new MessageModel();
        //非空判断
        if (StringUtil.isEmpty(name) || StringUtil.isEmpty(phoneNumber)) {
            messageModel.setCode(0);
            messageModel.setMessage("用户名或电话号码不能为空！");
            return messageModel;
        }
        boolean b = Pattern.matches("^1[3-9][0-9]{9}$", phoneNumber);
        if (!b){
            messageModel.setCode(0);
            messageModel.setMessage("电话号码格式错误！");
            return messageModel;
        }
        Client client = new Client();
        client.setName(name);
        client.setPhoneNumber(phoneNumber);

        Client client1;
        client1 = ClientDao.selectClientByPhoneNumber(phoneNumber);
        if (client1.getPhoneNumber() == null) {
            messageModel.setCode(0);
            messageModel.setMessage("登录失败！该号码未注册！");
            return messageModel;
        }

        if (!(client1.getName().equals(name))) {
            messageModel.setCode(0);
            messageModel.setMessage("登录失败！姓名或电话号码错误！");
            return messageModel;
        }

        //登录成功
        messageModel.setObject(client1);

        return messageModel;
    }

    public static MessageModel clientBooking(Integer clientID, Integer bookID, Integer number) throws SQLException, ClassNotFoundException {
        MessageModel messageModel = new MessageModel();
        Book book = BookDao.selectBookByID(bookID);
        if (book.getBookID() == null) {
            messageModel.setCode(0);
            messageModel.setMessage("预定失败！该图书编号不存在！");
            return messageModel;
        }
        BookingDao.insertBooking(clientID, bookID, number);
        messageModel.setCode(1);
        messageModel.setMessage("预定成功！可前往书店购买！");
        return messageModel;
    }

    public static MessageModel clientRegister(String name, String phoneNumber) throws SQLException, ClassNotFoundException {
        MessageModel messageModel = new MessageModel();
        //非空判断
        if (StringUtil.isEmpty(name) || StringUtil.isEmpty(phoneNumber)) {
            messageModel.setCode(0);
            messageModel.setMessage("用户名或电话号码不能为空！");
            return messageModel;
        }
        boolean b = Pattern.matches("^1[3-9][0-9]{9}$", phoneNumber);
        if (!b){
            messageModel.setCode(0);
            messageModel.setMessage("电话号码格式错误！");
            return messageModel;
        }
        Client client = ClientDao.selectClientByPhoneNumber(phoneNumber);
        if (client.getClientID() != null) {
            messageModel.setMessage("注册失败！该号码已被注册！");
        }
        ClientDao.insertClient(name, phoneNumber);
        messageModel.setMessage("注册成功！");
        return messageModel;
    }

    public static MessageModel deleteClient(Integer clientID) throws SQLException, ClassNotFoundException {
        MessageModel messageModel = new MessageModel();
        Client client = ClientDao.selectClientByID(clientID);
        if (client.getClientID() == null) {
            messageModel.setCode(0);
            messageModel.setMessage("删除失败！客户编号错误！");
        }
        ClientDao.deleteClient(clientID);
        messageModel.setMessage("删除成功！");
        return messageModel;
    }
}
