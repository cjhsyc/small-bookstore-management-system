package entity;

public class MessageModel {
    private Integer code = 1;//状态码，1：成功，0：失败
    private String message = "提示信息";//提示信息
    private Object object;//回显信息

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
