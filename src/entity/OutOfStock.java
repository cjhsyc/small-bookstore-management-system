package entity;

public class OutOfStock {
    private Integer outOfStockID;
    private Integer number;
    private Integer bookID;

    public Integer getOutOfStockID() {
        return outOfStockID;
    }

    public void setOutOfStockID(Integer outOfStockID) {
        this.outOfStockID = outOfStockID;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getBookID() {
        return bookID;
    }

    public void setBookID(Integer bookID) {
        this.bookID = bookID;
    }
}
